CC		= clang
NAME	= raytracer
CFLAGS	= -g0 -Ofast -ffast-math -march=native -mtune=native -W -Werror -Wextra -Wall
LIBS	= -lpthread -L./libft -lft -L/usr/X11/include -lmlx -lm -L/usr/X11/lib -lXext -lX11
CINCS	= -Iinc -Ilibft/inc/ -I/usr/X11/include/

FILES	= main			\
		  camera		\
		  checker		\
		  cmd_exit		\
		  cmd_info		\
		  cmd_obj		\
		  cmd_parser	\
		  cmd_render	\
		  cmd_select	\
		  cmd_set		\
		  color			\
		  cone			\
		  cylender		\
		  hook			\
		  keys			\
		  mandelbulb	\
		  matrix		\
		  mlx			\
		  obj			\
		  obj_func		\
		  par			\
		  perlin		\
		  plane			\
		  ray			\
		  render		\
		  sphere		\
		  transformer	\
		  triangle		\
		  util_pixel	\
		  util_cmd		\
		  util_color	\
		  util_perlin	\
		  util_ray		\
		  util_vec		\
		  vec			\
		  vec_math

SRC	= $(addsuffix .c, $(FILES))
SRC_PATH= $(addprefix src/, $(SRC))
OBJ		= $(SRC_PATH:src/%.c=obj/%.o)

.PHONY: all clean fclean re dirobj
.SILENT: dirobj clean fclean re $(NAME)

all: dirobj libft/libft.a $(NAME)

$(NAME): $(OBJ)
	echo "\033[33;4mCompiling binary:\033[0m"\
		"\033[32;2m\t$(shell pwd)/$(NAME)/\033[0m"
	@$(CC) $(OBJ) -o $(NAME) $(LIBS)
	echo "\033[33;3m$(NAME)\t\t\033[32;2mCreated\033[0m"

obj/%.o : src/%.c
	@$(CC) $(CFLAGS) $(CINCS) -o $@ -c $^

clean:
	test ! -d obj ||\
		echo "\033[31;4mDelete:\033[0m\033[32;2m\t\t\t$(shell pwd)/obj/\033[0m" && \
		rm -rf obj

fclean: clean
	test ! -f $(NAME) || \
		echo "\033[31;4mDelete:\033[0m\033[32;2m\t\t\t$(shell pwd)/$(NAME)/\033[0m" && \
		rm -f $(NAME)

sclean: fclean libft/clean

re: fclean all

dirobj:
	test -d obj || \
		echo "\033[33;4mCreate:\033[0m\033[32;2m\t\t\t$(shell pwd)/obj/\033[0m" && \
		mkdir -p obj

libft/libft.a:
	@make --no-print-directory -C ./libft

libft/clean:
	@test ! -f libft/libft.a || \
		make fclean --no-print-directory -C ./libft

run:
	./$(NAME)

test: re run
