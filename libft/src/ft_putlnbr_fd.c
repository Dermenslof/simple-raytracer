/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlnbr_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/27 16:55:26 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:52:16 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_putlnbr_rec_fd(long n, int fd)
{
	if (n < -9)
		ft_putlnbr_rec_fd(n / 10, fd);
	ft_putchar_fd('0' - n % 10, fd);
}

void					ft_putlnbr_fd(long n, int fd)
{
	if (n < 0)
		ft_putchar_fd('-', fd);
	else
		n = -n;
	ft_putlnbr_rec_fd(n, fd);
}
