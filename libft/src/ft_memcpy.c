/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 04:52:55 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:51:25 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void					*ft_memcpy(void *s1, const void *s2, size_t n)
{
	unsigned char		*ptr;
	unsigned const char	*ptr1;
	size_t				i;

	ptr = s1;
	ptr1 = s2;
	i = 0;
	while (i < n)
	{
		ptr[i] = ptr1[i];
		i++;
	}
	return (s1);
}
