
#include <stdlib.h>
#include <unistd.h>
#include "libft.h"

int			ft_printf(const char *expr, ...)
{
	va_list		args;
	char		*out;
	int			len;

	out = NULL;
	va_start(args, expr);
	parse(expr, &out, &args);
	va_end(args);
	if (!out)
		return (-1);
	len = write(1, out, ft_strlen(out));
	free(out);
	return (len);
}
