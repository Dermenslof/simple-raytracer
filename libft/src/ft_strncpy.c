/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:04:21 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:53:59 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strncpy(char *dest, const char *src, size_t n)
{
	size_t		i;
	size_t		s_len;

	i = 0;
	s_len = ft_strlen(src);
	while (i < n)
	{
		dest[i] = '\0';
		if (i < s_len)
			dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
