/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 00:55:11 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:54:08 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char			*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t		i;
	size_t		j;

	i = 0;
	if (!s2[0])
		return ((char *)s1);
	while (i < n && s1[i])
	{
		if (s1[i] == s2[0])
		{
			j = 1;
			while (i + j < n && s2[j])
			{
				if (s1[i + j] != s2[j])
					break ;
				j++;
			}
			if (!s2[j])
				return ((char*)&(s1[i]));
		}
		i++;
	}
	return (NULL);
}
