
#include <stdlib.h>
#include "libft.h"

static void	format_val_double(char **v, long par[6])
{
	char	*o;
	char	*d;
	char	*tmp;
	long	l;

	d = ft_strdup(ft_strchr(*v, '.'));
	tmp = *v;
	join_free(v, ft_strdup(""), ft_strsub(*v, 0, ft_strlen(*v) - ft_strlen(d)));
	free(tmp);
	l = ft_strlen(*v);
	if (par[2] >= 0 && par[2] != 6)
	{
		if (par[2] < 6)
			join_free(v, *v, ft_strsub(d, 0, (!par[2] ? 0 : par[2] + 1)));
		else
		{
			o = ft_memset(ft_strnew(par[2] - 6), '0', par[2] - 6);
			join_free(v, *v, d);
			join_free(v, *v, o);
		}
	}
	else
		join_free(v, *v, d);
}

static void	format_val_integer(char **v, char *f, long par[6])
{
	char	*o;
	char	*tmp;
	long	l;

	if ((l = ft_strlen(*v) - par[4] + par[5]) && par[2] > l && *f != 'f')
	{
		o = ft_memset(ft_strnew(par[2] - l), '0', par[2] - l);
		tmp = par[4] ? *v : NULL;
		join_free(v, o, (par[4] ? ft_strdup(*v + 1) : *v));
		free(tmp);
		if (par[4])
			join_free(v, ft_strdup("-"), *v);
	}
	if (par[3] && (*f == 'x' || *f == 'X' || *f == 'p' || *f == 'o'))
	{
		if (*f != 'o')
			join_free(v, (*f == 'x' ? ft_strdup("x") : ft_strdup("X")), *v);
		join_free(v, ft_strdup("0"), *v);
	}
}

static void	format_val_num(char **v, char *f, long par[6])
{
	long	l;

	if (*f != 'c' && *f != 's')
	{
		if (*f != 'f')
			format_val_integer(v, f, par);
		else
			format_val_double(v, par);
		l = ft_strlen(*v);
		par[1] = par[1] < l ? l : par[1];
		par[2] = par[2] < l ? l : par[2];
	}
}

void		format_val(char **val, char *flag, long par[6], char sp)
{
	long	len;
	char	*o;

	par[4] = **val == '-';
	par[5] = par[3] && *flag == 'o';
	format_val_num(val, flag, par);
	len = ft_strlen(*val);
	par[2] = par[2] > len ? len : par[2];
	(*val)[par[2]] = 0;
	len = par[1] - par[2];
	if (par[1] > 0 && (o = ft_strnew(len)))
		o = ft_memset(o, sp, len);
	if (par[0])
		join_free(val, *val, o);
	else
		join_free(val, o, *val);
}
