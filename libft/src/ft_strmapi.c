/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 21:04:25 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:53:48 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char				*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int				i;
	char			*tmp;

	i = -1;
	tmp = (char *)malloc(sizeof(char) * (ft_strlen(s) * 1));
	while (s[++i])
		tmp[i] = f((unsigned int)i, s[i]);
	tmp[i] = '\0';
	return (tmp);
}
