/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 01:26:23 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:54:12 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strrchr(const char *str, int c)
{
	char		ch;
	size_t		len;

	ch = (char)c;
	len = ft_strlen(str);
	while (len > 0)
	{
		if (str[len] == ch)
			return ((char *)&(str[len]));
		len--;
	}
	if (str[len] == ch)
		return ((char *)&(str[len]));
	return (NULL);
}
