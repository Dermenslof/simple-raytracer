
#include <stdlib.h>
#include "libft.h"

void		join_free(char **dst, char *s1, char *s2)
{
	char	*r;
	int		i;
	int		j;
	int		size;

	size = (s1 ? ft_strlen(s1) : 0) + (s2 ? ft_strlen(s2) : 0);
	r = malloc(sizeof(char) * (size + 1));
	i = 0;
	while (s1 && s1[i])
	{
		r[i] = s1[i];
		++i;
	}
	j = 0;
	while (s2 && s2[j])
	{
		r[i + j] = s2[j];
		++j;
	}
	r[i + j] = 0;
	free(s1);
	free(s2);
	*dst = r;
}

char		get_function(char c)
{
	char	tab[] = {'c', 's', 'd', 'i', 'x', 'X', 'p', 'o', 'f', 0};
	int		i;

	i = -1;
	while (tab[++i])
	{
		if (tab[i] == c)
			return (1);
	}
	return (0);
}

void		get_val(char **val, char *flag, va_list *args)
{
	if (*flag == 'c')
		get_char(val, va_arg(*args, unsigned int));
	else if (*flag == 's')
		get_string(val, va_arg(*args, char *));
	else if (*flag == 'd' || *flag == 'i')
		get_int(val, va_arg(*args, int));
	else if (*flag == 'x' || *flag == 'p')
		get_base(val, va_arg(*args, unsigned long), 16, 0);
	else if (*flag == 'X')
		get_base(val, va_arg(*args, unsigned long), 16, 32);
	else if (*flag == 'o')
		get_base(val, va_arg(*args, unsigned long), 8, 32);
	else if (*flag == 'f')
		get_double(val, va_arg(*args, double), 7);
}
