/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 05:01:09 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:51:39 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void					*ft_memmove(void *s1, const void *s2, size_t n)
{
	unsigned char		*ptr;
	unsigned const char	*ptr1;
	unsigned char		*tmp;
	size_t				i;

	ptr = s1;
	ptr1 = s2;
	i = 0;
	if (ptr + n < ptr1 || ptr1 + n < ptr)
		return (ft_memcpy(s1, s2, n));
	tmp = (unsigned char *)malloc(sizeof(unsigned char) * n);
	while (i < n)
	{
		tmp[i] = ptr1[i];
		++i;
	}
	i = 0;
	while (i < n)
	{
		ptr[i] = tmp[i];
		++i;
	}
	free(tmp);
	return (s1);
}
