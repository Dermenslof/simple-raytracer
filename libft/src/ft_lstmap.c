/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 18:46:54 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:50:58 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list			*list;
	t_list			*now;
	t_list			*tmp;
	t_list			*prev;

	list = f(lst);
	prev = list;
	tmp = lst->next;
	while (tmp)
	{
		now = f(tmp);
		tmp = tmp->next;
		prev->next = now;
		prev = now;
	}
	return (list);
}
