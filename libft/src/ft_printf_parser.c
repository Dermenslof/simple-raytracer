
#include <stdlib.h>
#include "libft.h"

static void	parse_opt(char **val, char *flag, char *opt)
{
	long	par[6];
	char	sp;

	par[2] = (*flag == 'f' ? -1 : ft_strlen(*val));
	if (((par[0] = 0) + 1) && *opt == '#' && ++opt)
		par[3] = 1;
	if (((par[1] = 0) + 1) && *opt == '-' && ++opt)
		par[0] = 1;
	if ((sp = ' ') && *opt == 0 && ++opt)
		sp = '0';
	if (((par[3] = 0) + 1) && ft_isdigit(*opt))
	{
		par[1] = ft_atol(opt);
		while (*opt >= '0' && *opt <= '9')
			++opt;
	}
	if (*opt == '.' && ++opt)
	{
		par[2] = ft_atol(opt);
		while (*opt >= '0' && *opt <= '9')
			++opt;
	}
	format_val(val, flag, par, sp);
}

static int	parse_flag(char *expr, char **out, va_list *args)
{
	int			len;
	char		*flag;
	char		*opt;
	char		*val;

	len = 0;
	flag = expr;
	opt = ft_memalloc(1);
	while (*flag && !ft_isalpha(*flag))
		++flag;
	if (ft_isalpha(*flag) && get_function(*flag))
	{
		get_val(&val, flag, args);
		if (flag != expr)
		{
			opt = ft_strsub(expr, 0, flag - expr);
			parse_opt(&val, flag, opt);
		}
		join_free(out, *out, val);
		len = ft_strlen(opt) + 1;
	}
	else
		join_free(out, *out, ft_strdup("%"));
	free(opt);
	return (len);
}

void		parse(const char *expr, char **out, va_list *args)
{
	char		*tmp;

	if (!expr)
		return ;
	if (!ft_strchr(expr, '%'))
	{
		*out = ft_strdup(expr);
		return ;
	}
	while ((tmp = ft_strchr(expr, '%')))
	{
		join_free(out, *out, ft_strsub(expr, 0, tmp - expr));
		expr = tmp + 1;
		expr += parse_flag((char *)expr, out, args);
	}
	join_free(out, *out, ft_strdup(expr));
}
