
#include <stdlib.h>
#include "libft.h"

void			get_base(char **val, unsigned long n, long b, int maj)
{
	static char	hex[] = {'a', 'b', 'c', 'd', 'e', 'f', 0};
	long		div;
	long		tmp;
	long		pdiv;
	long		cnt;

	cnt = 0;
	div = 1;
	while (n / div > (unsigned long)b)
		div *= b;
	pdiv = div;
	while (pdiv && ++cnt)
		pdiv /= b;
	*val = ft_strnew(cnt);
	pdiv = 0;
	while (div)
	{
		tmp = n / div % b;
		if (b == 16 && tmp > 9)
			(*val)[pdiv++] = hex[tmp - 10] - maj;
		else
			(*val)[pdiv++] = '0' + tmp;
		div /= b;
	}
}

void			get_char(char **val, unsigned int c)
{
	if ((*val = malloc(sizeof(char) * 2)))
	{
		**val = c;
		(*val)[1] = 0;
	}
}

void			get_int(char **val, int i)
{
	*val = ft_itoa(i);
}

void			get_string(char **val, char *str)
{
	*val = ft_strdup(str);
}
