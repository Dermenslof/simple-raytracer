/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 07:05:17 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:51:14 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void					*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned char		*ptr;
	unsigned char		*ptr1;
	unsigned char		ch;
	size_t				i;

	ptr = (unsigned char*)s1;
	ptr1 = (unsigned char*)s2;
	ch = (unsigned char)c;
	i = -1;
	while (++i < n)
	{
		ptr[i] = ptr1[i];
		if (ptr1[i] == ch)
			return ((void*)&ptr[i + 1]);
	}
	return (NULL);
}
