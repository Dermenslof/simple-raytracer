/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlnbr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/27 16:53:55 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:52:10 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void				ft_putlnbr_rec(long n)
{
	if (n < -9)
		ft_putlnbr_rec(n / 10);
	ft_putchar('0' - n % 10);
}

void					ft_putlnbr(long n)
{
	if (n < 0)
		ft_putchar('-');
	else
		n = -n;
	ft_putlnbr_rec(n);
}
