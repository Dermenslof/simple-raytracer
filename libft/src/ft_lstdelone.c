/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 17:15:37 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:50:51 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void			ft_lstdelone(t_list **alst, void (*del)(void *, size_t))
{
	del(*alst, (*alst)->content_size);
	(*alst)->content_size = 0;
	free(*alst);
	*alst = NULL;
}
