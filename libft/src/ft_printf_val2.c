
#include "libft.h"

static void		get_floating(char **val, double n, long pos)
{
	int			i;
	int			carry;
	double		tmp;

	tmp = n;
	(*val)[pos++] = '.';
	i = -1;
	while (++i < 6)
	{
		n *= 10;
		(*val)[pos++] = '0' + (long)n;
		n -= (long)n;
	}
	carry = (double)ft_atoi(&(*val)[pos - 6]) / 1000000.0 != tmp;
	i = -1;
	--pos;
	while (++i < 6)
	{
		(*val)[pos - i] = (*val)[pos - i] - '0' + carry;
		carry = (*val)[pos - i] / 10;
		(*val)[pos - i] %= 10;
		(*val)[pos - i] += '0';
	}
}

void			get_double(char **val, double n, long cnt)
{
	double		div;
	double		sub;
	double		pdiv;
	long		tmp;
	char		neg;

	neg = (n < 0);
	n = !neg ? -n : n;
	div = 1;
	while (n / div <= -10 && !(sub = 0))
		div *= 10;
	pdiv = div;
	while (pdiv >= 1 && ++cnt)
		pdiv /= 10;
	*val = ft_strnew(cnt + neg);
	if (neg)
		(*val)[(long)pdiv++] = '-';
	while (div >= 1)
	{
		tmp = (long)(n / div) % 10;
		(*val)[(long)pdiv++] = '0' - tmp;
		sub = sub * 10 - tmp;
		div /= 10;
	}
	get_floating(val, (n + sub) * -1, (long)pdiv);
}
