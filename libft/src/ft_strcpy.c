/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fmontaro <fmontaro@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 16:58:09 by fmontaro          #+#    #+#             */
/*   Updated: 2014/05/13 04:52:58 by fmontaro         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char			*ft_strcpy(char *dest, const char *src)
{
	char		*d;

	d = dest;
	while (*src)
		*(dest++) = *(src++);
	*dest = 0;
	return (d);
}
