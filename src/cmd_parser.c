
#include "rtv1.h"

extern t_par		*g_par;

static t_cmd		cmds[] = {
	{"add", &cmd_add},
	{"render", &cmd_render},
	{"select", &cmd_select},
	{"unselect", &cmd_unselect},
	{"info", &cmd_info},
	{"delete", &cmd_delete},
	{"set", &cmd_set},
	{"exit", &cmd_exit},
	{NULL, NULL}
};

static char		*g_status[] = {
	"first pass",
	"effects",
	"anti-aliasing",
};

void				parse_cmd(int ac, char **av)
{
	int		i;

	i = -1;
	while (ac && cmds[++i].name)
	{
		if (!ft_strcmp(av[0], cmds[i].name))
		{
			if (g_par->status && ft_strcmp(av[0], "info")
					&& ft_strcmp(av[0], "exit"))
			{
				ft_printf("%s", T_ERROR);
				ft_printf("current rendering %s, please wait\n",
						g_status[g_par->status]);
				return;
			}
			cmds[i].func(ac, av);
			return ;
		}
	}
	if (ac)
		ft_printf("%s\"%s\" command not found.\n", T_ERROR, *av);
}
