
#include "rtv1.h"

t_par		*g_par;

void		run_cmd(t_par *p)
{
	char	buf[256];
	int		ret;
	char	**av;

	(void)p;
	ft_printf("%s\n%s", T_CLEAR, T_PROMPT);
	while ((ret = read(0, buf, 255)) >= 0)
	{
		buf[ret - 1] = 0;
		av = ft_strsplit(buf, ' ');
		if (av && *av)
			parse_cmd(count_split(av), av);
		free_split(av);
		if (!g_par->do_select)
			ft_printf(T_PROMPT);
	}
	exit(0);
}

int			main(void)
{
	pthread_t		t;

	init_par();
	pthread_create(&t, NULL, (void*)&run_cmd, g_par);
	pthread_detach(t);
	init_mlx();
	return (0);
}
