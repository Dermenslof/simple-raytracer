
#include "rtv1.h"

float			iplane(t_vec *i, t_vec ro, t_vec rd, t_obj *o)
{
	float	d;

	apply_transform(o->itrans, &ro, &rd);
	d = -ro.y / rd.y;
	if (rd.y == 0.0 || d <= EPSILON)
		return (-1.0);
	*i = add(ro, scale(rd, d));
	i->y = 0.0;
	i->w = 1.0;
	d = length(sub(ro, *i));
	apply_transform(o->trans, i, &ro);
	return (d);
}

t_vec			nplane(t_vec i, t_vec rd, t_obj *o)
{
	t_vec	n;

	apply_transform(o->itrans, &i, NULL);
	n = vec(0.0, 1.0, 0.0);
	return (correct(i, rd, o, n));
}
