
#include "rtv1.h"

extern t_par	*g_par;

void			plot(int x, int y, t_vec c)
{
	char		*img;

	if (x < 0 || y < 0 || x >= g_par->w || y >= g_par->h)
		return ;
	img = g_par->bgd + x * g_par->bpp / 8 + y * g_par->sl;
	img[0] = (int)(c.z * 255.0);
	img[1] = (int)(c.y * 255.0);
	img[2] = (int)(c.x * 255.0);
}

void			plot2(int x, int y, t_vec c)
{
	char		*img;

	if (x < 0 || y < 0 || x >= g_par->w || y >= g_par->h)
		return ;
	img = g_par->bgd2 + x * g_par->bpp / 8 + y * g_par->sl;
	img[0] = (int)(c.z * 255.0);
	img[1] = (int)(c.y * 255.0);
	img[2] = (int)(c.x * 255.0);
}

t_vec					get_pixel_color(int x, int y)
{
	char	*pix;
	t_vec	col;

	pix = g_par->bgd2 + x * g_par->bpp / 8 + y * g_par->sl;
	col.x = (pix[2] & 255) / 255.0;
	col.y = (pix[1] & 255) / 255.0;
	col.z = (pix[0] & 255) / 255.0;
	return (col);
}

void					mark_img(int x, int y, t_vec c, int id)
{
	char		*img;
	char		select;

	if (x < 0 || y < 0 || x >= g_par->w || y >= g_par->h)
		return ;
	select = (g_par->id == id);
	img = g_par->bgd + x * g_par->bpp / 8 + y * g_par->sl;
	img[0] = (int)(c.z * 255.0);
	img[1] = (int)(c.y * 255.0);
	img[2] = (int)(c.x * 255.0);
	if (id > -2)
		img[3] = select;
}
