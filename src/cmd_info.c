
#include "rtv1.h"

extern t_par		*g_par;

static char			*g_types[] = {
	"LIGHT",
	"SPHERE",
	"PLANE",
	"CONE",
	"CYLENDER",
	"TRIANGLE",
	"MANDELBULB",
	NULL
};

void				print_list(void)
{
	t_obj	*o;

	o = g_par->lights;
	while (o)
	{
		ft_printf("%d -> %s\n", o->id, g_types[o->type]);
		o = o->next;
	}
	o = g_par->objects;
	while (o)
	{
		ft_printf("%d -> %s\n", o->id, g_types[o->type]);
		o = o->next;
	}
}

void				cmd_info(int ac, char **av)
{
	if (ac == 2 && !ft_strcmp("list", av[1]))
	{
		print_list();
		return ;
	}
	if (g_par->id)
		print_object((get_obj(g_par->id)), 0);
	else
		ft_printf("%sno object selected.\n", T_ERROR);
}

void				print_object(t_obj *o, char prompt)
{
	ft_printf("type: %s\n", g_types[o->type]);
	ft_printf("position: {%.4f, %.4f, %.4f}\n", o->pos.x, o->pos.y, o->pos.z);
	ft_printf("direction: {%.4f, %.4f, %.4f}\n", o->dir.x, o->dir.y, o->dir.z);
	ft_printf("color%s: {%.4f, %.4f, %.4f}\n", o->type ? " 1" : "", o->col.x,
			o->col.y, o->col.z);
	if (o->type)
	{
		ft_printf("color 2: {%.4f, %.4f, %.4f}\n", o->col2.x, o->col2.y,
				o->col2.z);
		ft_printf("radius: %.4f\n", o->r);
		ft_printf("Ka: %.4f\n", o->ka);
		ft_printf("Kt: %.4f\n", o->kt);
		ft_printf("Kd: %.4f\n", o->kd);
		ft_printf("Ks: %.4f\n", o->ks);
		ft_printf("Kr: %.4f\n", o->kr);
		ft_printf("Kl: %d\n", (int)o->kl);
		ft_printf("n: %.4f\n", o->n);
		ft_printf("Kch: %.1f\n", o->kch);
		ft_printf("Ktp: %d\n", o->ktp);
	}
	if (prompt)
		ft_printf(T_PROMPT);
}
