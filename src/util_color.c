
#include "rtv1.h"

extern t_par	*g_par;

t_vec			clamp(t_vec c, float min, float max)
{
	if (c.x < min)
		c.x = min;
	if (c.x > max)
		c.x = max;
	if (c.y < min)
		c.y = min;
	if (c.y > max)
		c.y = max;
	if (c.z < min)
		c.z = min;
	if (c.z > max)
		c.z = max;
	return (c);
}

float		clamp1(float t, float min, float max)
{
	if (t < min)
		t = min;
	if (t > max)
		t = max;
	return (t);
}
