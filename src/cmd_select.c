
#include "rtv1.h"

extern t_par		*g_par;

void				render_select(void)
{
	int		x;
	int		y;
	int		sl;
	char	*p;

	sl = g_par->sl;
	y = -1;
	while (++y < g_par->h)
	{
		x = -1;
		while (++x < g_par->w)
		{
			p = g_par->bgd + x * g_par->bpp / 8 + y * sl;
			if (p[3])
			{
				if (!x || !y || x == g_par->w - 1 || y == g_par->h - 1)
					p[2] = 255;
				else if (!((p - 4)[3]) || !((p + 4)[3]))
					p[2] = 255;
				else if (!((p - sl)[3]) || !((p + sl)[3]))
					p[2] = 255;
			}
		}
	}
}

void				cmd_subselect(int x, int y)
{
	g_par->id = select_obj(x, y);
	g_par->do_select = 0;
	if (!g_par->id)
		ft_printf("%sobject not found.\n%s", T_ERROR, T_PROMPT);
	else
		ft_printf(T_PROMPT);
	g_par->render = 1;
}

void				cmd_select(int ac, char **av)
{
	t_obj		*o;
	int			id;

	if (ac == 1)
	{
		ft_printf("please select an object with your mouse.\n");
		g_par->do_select = 1;
		g_par->id = 0;
	}
	else if (ac == 2)
	{
		id = ft_atoi(av[1]);
		if (!(o = get_obj(id)))
			ft_printf("%sobject \"%s\" not found.\n", T_ERROR, av[1]);
		else
		{
			g_par->id = id;
			g_par->render = 1;
		}
	}
	else
		ft_printf("usage: %s [objectId]\n", *av);
}

void				cmd_unselect(int ac, char **av)
{
	(void)ac;
	(void)av;
	g_par->do_select = 0;
	g_par->id = 0;
	g_par->render = 1;
}
