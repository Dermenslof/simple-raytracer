
#include "rtv1.h"

extern t_par		*g_par;

float					get_intersect(t_vec *i, t_vec ro, t_vec rd, t_obj *o)
{
	float		t;

	t = -1.0;
	if (o->type == SPHERE)
		t = isphere(i, ro, rd, o);
	else if (o->type == PLANE)
		t = iplane(i, ro, rd, o);
	else if (o->type == CYLENDER)
		t = icylender(i, ro, rd, o);
	else if (o->type == CONE)
		t = icone(i, ro, rd, o);
	else if (o->type == TRIANGLE)
		t = itriangle(i, ro, rd, o);
	else if (o->type == MANDELBULB)
		t = imandelbulb(i, ro, rd, o);
	return (t);
}

t_vec					get_normal(t_vec i, t_vec rd, t_obj *o)
{
	t_vec		n;

	n = vec(0.0, 0.0, 0.0);
	if (o->type == SPHERE)
		n = nsphere(i, rd, o);
	else if (o->type == PLANE)
		n = nplane(i, rd, o);
	else if (o->type == CYLENDER)
		n = ncylender(i, rd, o);
	else if (o->type == CONE)
		n = ncone(i, rd, o);
	else if (o->type == TRIANGLE)
		n = ntriangle(i, rd, o);
	else if (o->type == MANDELBULB)
		n = nmandelbulb(i, rd, o);
	return (n);
}

t_vec			correct(t_vec i, t_vec rd, t_obj *o, t_vec n)
{
	(void)i;
	apply_transform(o->itrans, &rd, NULL);
	if (dot(n, rd) > 0.0)
		n = inv(n);
	apply_transform(o->trans, &n, NULL);
	return (norm(n));
}
