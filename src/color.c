
#include "rtv1.h"

extern t_par	*g_par;

t_vec			get_color(t_vec i, t_obj *o)
{
	if (o->ktp == 1)
		return (get_checker(i, o));
	if (o->ktp > 1)
		return (perlin_color(i, o));
	return (o->type == MANDELBULB ? get_color_mb(o) : o->col);
}

t_vec			process_direct(t_vec i, t_vec n, t_obj *l, t_obj *o)
{
	float		ps1;
	float		ps2;
	t_vec		ld;
	t_vec		nld;
	t_vec		c;

	c = vec(0.0, 0.0, 0.0);
	ld = norm(sub(l->pos, i));
	nld = norm(sub(i, l->pos));
	ps1 = dot(n, ld);
	if (ps1 > 0.0)
	{
		c = scale(get_color(i, o), o->kd * ps1);
		ps2 = -dot(n, nld);
		if (ps2 > 0.0)
			c = add(c, scale(l->col, o->ks * pow(ps2, o->kl)));
	}
	return (clamp(c, 0.0, 1.0));
}

char			is_lightning(t_obj *ol, t_vec i)
{
	t_vec		ld;
	t_vec		vt;
	t_vec		pbt;
	float		t;
	t_obj		*ot;

	pbt = ol->pos;
	if (g_par->softshadow != 0.0)
	{
		pbt.x += RANDSHADOW(g_par->softshadow);
		pbt.y += RANDSHADOW(g_par->softshadow);
		pbt.z += RANDSHADOW(g_par->softshadow);
	}
	ld = norm(sub(pbt, i));
	pbt = sub(i, pbt);
	ot = g_par->objects;
	while (ot)
	{
		t = get_intersect(&vt, i, ld, ot);
		if (t < length(pbt) && t > EPSILON)
			return (0);
		ot = ot->next;
	}
	return (1);
}

t_vec			get_direct(t_vec i, t_vec n, t_obj *o)
{
	t_vec		c;
	t_obj		*tmp;

	c = vec(0.0, 0.0, 0.0);
	tmp = g_par->lights;
	while (tmp)
	{
		if (is_lightning(tmp, i))
			c = add(c, process_direct(i, n, tmp, o));
		tmp = tmp->next;
	}
	return (clamp(c, 0.0, 1.0));
}

t_vec			get_pixel(t_vec i, t_vec rd, t_obj *o, int pass, char inside)
{
	t_vec		n;
	t_vec		c;
	int			j;

	n = get_normal(i, rd, o);
	c = scale(get_color(i, o), (1.0 - o->kt) * o->ka);
	j = -1;
	/*
	 *while (++j < (g_par->status == 2 ? 4 : 1))
	 *    c = add(c, scale(get_direct(i, n, o), (g_par->status == 2 ? 0.25 : 1.0)));
	 */
	if (o->kd > 0.0 || o->ks > 0.0)
		c = add(c, get_direct(i, n, o));
	if (o->kr > 0.0)
		c = add(c, scale(trace(i, reflect(rd, n), pass + 1, inside), o->kr));
	if (o->kt > 0.0)
	{
		rd = refract(rd, n, o->n, &inside);
		c = add(c, scale(trace(i, rd, pass + 1, inside), o->kt));
	}
	return (clamp(c, 0.0, 1.0));
}

t_vec			get_first_pixel(t_vec i, t_vec rd, t_obj *o, t_vec *buf)
{
	t_vec		n;
	t_vec		c;
	int			j;
	char		inside;

	inside = 0;
	buf[0] = i;
	buf[1] = vec(0.0, 0.0, 0.0);
	buf[2] = vec(0.0, 0.0, 0.0);
	n = get_normal(i, rd, o);
	c = scale(get_color(i, o), (1.0 - o->kt) * o->ka);
	j = -1;
	c = add(c, get_direct(i, n, o));
	if (o->kr > 0.0)
		buf[1] = reflect(rd, n);
	if (o->kt > 0.0)
		buf[2] = refract(rd, n, o->n, &inside);
	return (clamp(c, 0.0, 1.0));
}
