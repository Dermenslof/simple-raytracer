
#include "rtv1.h"

float			icone(t_vec *i, t_vec ro, t_vec rd, t_obj *o)
{
	float	a;
	float	b;
	float	c;
	float	d;

	(void)i;
	apply_transform(o->itrans, &ro, &rd);
	a = pow(rd.x, 2) + pow(rd.z, 2) - o->r * pow(rd.y, 2);
	b = (ro.x * rd.x + ro.z * rd.z - o->r * ro.y * rd.y);
	c = pow(ro.x, 2) + pow(ro.z, 2) - o->r * pow(ro.y, 2);
	d = b * b - a * c;
	if (d <= EPSILON)
		return (-1.0);
	d = (-b - sqrt(d)) / a;
	c = (-b + sqrt(d)) / a;
	if (c <= EPSILON && d <= EPSILON)
		return (-1.0);
	a = 0.0;
	if ((c <= d && c > EPSILON) || (d < c && d < EPSILON))
		a = c;
	else if ((d < c && d > EPSILON) || (c < d && c < EPSILON))
		a = d;
	*i = add(ro, scale(rd, a));
	i->w = 1.0;
	a = length(sub(ro, *i));
	apply_transform(o->trans, i, &ro);
	return (a);
}

t_vec			ncone(t_vec i, t_vec rd, t_obj *o)
{
	t_vec		n;

	apply_transform(o->itrans, &i, NULL);
	n = i;
	n.y = -n.y * tan(o->r);
	n.w = 0.0;
	return (correct(i, rd, o, n));
}
