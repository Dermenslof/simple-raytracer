
#include "rtv1.h"

extern t_par		*g_par;

void				init_coef(t_obj *o)
{
	o->height = 0.5;
	o->r = 1.0;
	o->ka = 0.15;
	o->kd = 0.5;
	o->ks = 0.5;
	o->kl = 42.0;
	o->kr = 0.0;
	o->kt = 0.0;
	o->n = 1.0;
	o->kch = -1.0;
	o->ktp = 0;
}

t_obj				*new_obj(int type)
{
	static int	ids = 1;
	t_obj		*o;

	if ((o = malloc(sizeof(t_obj))))
	{
		o->id = ids++;
		o->type = type;
		o->pos = vec(0.0, 0.0, 0.0);
		o->pos2 = vec(0.0, 0.0, 0.0);
		o->pos3 = vec(0.0, 0.0, 0.0);
		o->col = vec(1.0, 1.0, 1.0);
		o->col2 = vec(0.0, 0.0, 0.0);
		o->dir = vec(0.0, 0.0, 0.0);
		o->scale = vec(1.0, 1.0, 1.0);
		o->nor = vec(0.0, 0.0, 0.0);
		init_coef(o);
		set_transform(o);
		set_itransform(o);
		o->next = NULL;
	}
	return (o);
}

void				add_obj(t_obj **base, t_obj *o)
{
	t_obj		*tmp;

	tmp = *base;
	if (!tmp)
	{
		*base = o;
		return ;
	}
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = o;
}

void				free_objs(t_obj **o)
{
	t_obj		*tmp;

	tmp = *o;
	if (tmp->next)
		free_objs(&tmp->next);
	free(*o);
	*o = NULL;
}

t_obj				*pop_obj(t_obj **base, int id)
{
	t_obj		*tmp;
	t_obj		*d;

	tmp = *base;
	if (tmp && tmp->id == id)
	{
		*base = tmp->next;
		return (tmp);
	}
	else if (tmp)
	{
		while (tmp->next)
		{
			if (tmp->next->id == id)
			{
				d = tmp->next;
				tmp->next = d->next;
				return (d);
			}
			tmp = tmp->next;
		}
	}
	return (NULL);
}
