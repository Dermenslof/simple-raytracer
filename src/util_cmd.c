
#include "rtv1.h"

extern t_par		*g_par;

int					select_obj(int x, int y)
{
	t_vec	ro;
	t_vec	rd;
	t_vec	i;
	t_obj	*o;
	float	t;

	move_cam(x, y, &ro, &rd);
	o = test_intersect(&i, ro, rd, &t);
	if (o)
		return (o->id);
	return (0);
}

t_obj				*get_obj(int id)
{
	t_obj		*o;

	o = g_par->lights;
	while (o)
	{
		if (o->id == id)
			return (o);
		o = o->next;
	}
	o = g_par->objects;
	while (o)
	{
		if (o->id == id)
			return (o);
		o = o->next;
	}
	return (NULL);
}

void			free_split(char **split)
{
	int		i;

	if (!split)
		return ;
	i = -1;
	while (split[++i])
		free(split[i]);
	free(split);
}

int				count_split(char **split)
{
	int		i;

	i = 0;
	while (split && split[i])
		++i;
	return (i);
}

float			ft_atof(char *str, int i)
{
	float	dec;
	float	div;
	float	res;
	float	neg;

	res = 0.0;
	dec = 0.0;
	neg = 1.0;
	while (str[i] && ((str[i] >= 9 && str[i] <= 13) || str[i] == ' '))
		i++;
	if (str[i] == '-' || str[i] == '+')
		neg = str[i++] == '-' ? -1.0 : 1.0;
	while (str[i] && str[i] >= '0' && str[i] <= '9')
		res = res * 10.0 + (str[i++] - '0');
	if (str[i++] != '.')
		return ((dec + res) * neg);
	div = 1;
	while (str[i] && str[i] >= '0' && str[i] <= '9')
	{
		dec = dec * 10.0 + (str[i++] - '0');
		div *= 10.0;
	}
	dec /= div;
	return ((dec + res) * neg);
}
