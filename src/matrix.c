
#include "rtv1.h"

void			print_matrix(t_mat *m)
{
	ft_printf("\n");
	ft_printf("%f %f %f %f\n", m->v[0], m->v[1], m->v[2], m->v[3]);
	ft_printf("%f %f %f %f\n", m->v[4], m->v[5], m->v[6], m->v[7]);
	ft_printf("%f %f %f %f\n", m->v[8], m->v[9], m->v[10], m->v[11]);
	ft_printf("%f %f %f %f\n", m->v[12], m->v[13], m->v[14], m->v[15]);
	ft_printf("\n");
}

void			matrix_id(t_mat *m)
{
	int		i;

	i = -1;
	while (++i < 16)
		m->v[i] = (i % 5 == 0 ? 1.0 : 0.0);
}

void			matrix_apply(t_vec *v, t_mat a, t_vec b)
{
	v->x = a.v[0] * b.x + a.v[1] * b.y + a.v[2]  * b.z + a.v[3] * b.w;
	v->y = a.v[4] * b.x + a.v[5] * b.y + a.v[6]  * b.z + a.v[7] * b.w;
	v->z = a.v[8] * b.x + a.v[9] * b.y + a.v[10] * b.z + a.v[11] * b.w;
	v->w = a.v[12] * b.x + a.v[13] * b.y + a.v[14] * b.z + a.v[15] * b.w;
	if (v->w != 0.0)
	{
		*v = scale(*v, 1.0 / v->w);
		v->w /= v->w;
	}
}

void			matrix_translate(t_mat *m, t_vec v)
{
	matrix_id(m);
	m->v[3] = v.x;
	m->v[7] = v.y;
	m->v[11] = v.z;
}

void			matrix_scale(t_mat *m, t_vec v)
{
	matrix_id(m);
	m->v[0] = v.x;
	m->v[5] = v.y;
	m->v[10] = v.z;
}

void			matrix_itranslate(t_mat *m, t_vec v)
{
	matrix_translate(m, inv(v));
}

void			matrix_iscale(t_mat *m, t_vec v)
{
	matrix_scale(m, divide(vec(1.0, 1.0, 1.0), v));
}

void			matrix_rotx(t_mat *m, float a)
{
	matrix_id(m);
	m->v[5] = cos(a);
	m->v[6] = -sin(a);
	m->v[9] = sin(a);
	m->v[10] = cos(a);
}

void			matrix_roty(t_mat *m, float a)
{
	matrix_id(m);
	m->v[0] = cos(a);
	m->v[2] = sin(a);
	m->v[8] = -sin(a);
	m->v[10] = cos(a);
}

void			matrix_rotz(t_mat *m, float a)
{
	matrix_id(m);
	m->v[0] = cos(a);
	m->v[1] = -sin(a);
	m->v[4] = sin(a);
	m->v[5] = cos(a);
}

void			matrix_mult(t_mat *m, t_mat m1, t_mat m2)
{
	int			i;
	int			j;
	int			k;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			m->v[4 * i + j] = 0.0;
			k = -1;
			while (++k < 4)
				m->v[4 * i + j] += m1.v[4 * i + k] * m2.v[4 * k + j];
		}
	}
}

void			matrix_rot(t_mat *m, t_vec v)
{
	t_mat	mr;
	t_mat	tmp;
	t_mat	tmp2;

	matrix_id(m);
	matrix_id(&mr);
	v = scale(v, M_PI / 180.0);
	matrix_rotz(&mr, v.z);
	matrix_mult(&tmp, *m, mr);
	matrix_roty(&mr, v.y);
	matrix_mult(&tmp2, tmp, mr);
	matrix_rotx(&mr, v.x);
	matrix_mult(m, tmp2, mr);
}

void			matrix_irot(t_mat *m, t_vec v)
{
	t_mat	mr;
	t_mat	tmp;
	t_mat	tmp2;

	matrix_id(m);
	v = scale(inv(v), M_PI / 180.0);
	matrix_id(&mr);
	matrix_rotx(&mr, v.x);
	matrix_mult(&tmp, *m, mr);
	matrix_roty(&mr, v.y);
	matrix_mult(&tmp2, tmp, mr);
	matrix_rotz(&mr, v.z);
	matrix_mult(m, tmp2, mr);
}
