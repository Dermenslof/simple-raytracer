
#	include "rtv1.h"

extern t_par	*g_par;

t_vec					get_uv(int x, int y)
{
	t_vec			uv;
	float			e;
	float			ratio;
	static float	fov = 60.0;

	e = tan((fov / 2.0) * M_PI / 180.0);
	ratio = (float)g_par->w / g_par->h;
	uv = vec((float)x, (float)-y, 0.0);
	if (g_par->status == 2)
		uv = sub(uv, vec((float)random() / (float)INT_MAX
					, -(float)random() / (float)INT_MAX, 0.0));
	uv = divide(uv, vec((float)g_par->w, (float)g_par->h, 1.0));
	uv = add(vec(-1.0, 1.0, 0.0), scale(uv, 2.0));
	uv = mult(uv, vec(e, e / ratio, 0.0));
	return (uv);
}

t_obj					*test_intersect(t_vec *i, t_vec ro, t_vec rd, float *t)
{
	t_obj		*tmp;
	t_obj		*o;
	t_vec		it;
	float		ti;

	*t = -1.0;
	o = NULL;
	tmp = g_par->objects;
	while (tmp)
	{
		ti = get_intersect(&it, ro, rd, tmp);
		if (ti > 0.0 && (ti < *t || *t < 0.0))
		{
			o = tmp;
			*i = it;
			*t = ti;
		}
		tmp = tmp->next;
	}
	return (o);
}

t_vec			refract(t_vec rd, t_vec nor, float n, char *inside)
{
	float		dev;
	float		ti;
	float		tt;
	float		coef;
	float		ps;

	*inside = ((n == 1.0) ? 0 : ((*inside == 0) ? 1 : 0));
	dev = ((*inside == 1) ? 1.0 / n : n);
	ps = dot(rd, nor);
	ti = acos(ps);
	tt = asin(dev * sin(ti));
	coef = cos(tt) + dev * ps;
	rd = sub(scale(rd, dev), scale(nor, coef));
	return (rd);
}

t_vec					reflect(t_vec rd, t_vec n)
{
	return (sub(rd, scale(n, 2.0 * dot(rd, n))));
}
