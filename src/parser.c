
#include "rtv1.h"

static t_cmd	funcs[] = {
	{"global", &set_global},
	{"camera", &set_camera},
	{"object", &set_object},
	{"light", &set_light},
	{NULL, NULL}
};

void			parse_global(char *line)
{
	char	**tab;

	tab = ft_strsplit(line, ':');
	if (count_split(tab) == 2)
	{
		if (!ft_strcmp("aa", *tab))
			g_par->aa = ft_atoi(tab[1]);
		else if (!ft_strcmp("threshold", *tab))
			g_par->threshold = ft_atof(tab[1], 0);
		else if (!ft_strcmp("softshadow", *tab))
			g_par->softshadow = ft_atof(tab[1], 0);
		else if (!ft_strcmp("nbshadow", *tab))
			g_par->nbshadow = ft_atoi(tab[1]);
		else if (!ft_strcmp("bg-color", *tab))
			g_par->bg_color = get_triplet(tab[1]);
	}
	free_split(tab);
}

void			set_global(int fd)
{
	int		ret;
	int		i;
	char	*line;
	char	c;

	line = NULL;
	while ((ret = get_next_line(fd, &line)) && line)
	{
		if (ret < 0)
			break ;
		c = *line;
		if (c != '{')
			parse_global(line);
		free(line);
		line = NULL;
		if (c == '}')
			break ;
	}
}

void			parse_scene(char *file)
{
	int		fd;
	int		ret;
	int		i;
	char	*line;

	if ((fd = open(file, O_RDONLY)) < 0)
		return ;
	line = NULL;
	while ((ret = get_next_line(fd, &line)) && line)
	{
		if (ret < 0)
			break ;
		i = -1;
		while (funcs[++i])
		{
			if (!ft_strmp(funcs[i].name, line))
				funcs[i].func(fd)
		}
		free(line);
		line = NULL;
	}
	close(fd);
}
