
#include "rtv1.h"

extern t_par	*g_par;

void			ry(t_vec *p, float a)
{
	t_vec		q;
	float		c;
	float		s;

	q = *p;
	c = cos(a);
	s = sin(a);
	p->x = c * q.x + s * q.z;
	p->z = -s * q.x + c * q.z;
}

t_vec			mb(t_vec p, t_obj *o)
{
	t_vec		z;
	float		dr;
	float		t0;
	float		a[3];
	int			i;

	p = vec(p.x, p.z, p.y);
	z = p;
	dr = 1.0;
	t0 = 1.0;
	i = -1;
	while (++i < 7)
	{
		*a = length(z);
		if (*a > 2.0)
			continue ;
		a[1] = atan(z.y / z.x);
		a[2] = asin(z.z / *a);
		dr = powf(*a, o->r - 1.0) * dr * o->r + 1.0;
		*a = powf(*a, o->r);
		a[1] *= o->r;
		a[2] *= o->r;
		z = add(scale(vec(cos(a[1]) * cos(a[2]), sin(a[1]) * cos(a[2]), sin(a[2])), *a), p);
		t0 = fmin(t0, *a);
	}
	return (vec(0.5 * log(*a) * *a / dr, t0, 0.0));
}

t_vec			f(t_vec p, t_obj *o)
{
	ry(&p, 0.5);
	return (mb(p, o));
}

float			imandelbulb(t_vec *it, t_vec ro, t_vec rd, t_obj *o)
{
	float	t, res_t, res_d, max_error, d, pd, os, step, error;
	t_vec	c, res_c;
	int		i;

	(void)it;
	ro = sub(ro, o->pos);
	t = 1.0;
	res_t = 0.0;
	res_d = 1000.0;
	max_error = 1000.0;
	d = 1.0;
	pd = 100.0;
	os = 0.0;
	step = 0.0;
	error = 1000.0;
	i = -1;
	while (++i < 42)
	{
		if (error >= 0.5 * (1.0 / (g_par->w * 3.0)) && t <= 20.0)
		{
			c = f(add(ro, scale(rd, t)), o);
			d = c.x;
			if (d > os)
			{
				os = 0.4 * d * d / pd;
				step = d + os;
				pd = d;
			}
			else
			{
				step = -os;
				os = 0.0;
				pd = 100.0;
				d = 1.0;
			}
			error = d / t;
			if (error < max_error)
			{
				max_error = error;
				res_t = t;
				res_c = c;
			}
			t += step;
		}
	}
	if (t > 20.0)
		res_t = -1.0;
	o->dir.x = res_t;
	o->dir.y = res_c.y;
	o->dir.z = res_c.z;
	/*return (vec(res_t, res_c.y, res_c.z));*/
	return (res_t <= EPSILON ? -1.0 : res_t);

}

t_vec			nmandelbulb(t_vec i, t_vec rd, t_obj *o)
{
	t_vec		eps1;
	t_vec		eps2;
	t_vec		n;

	(void)rd;
	eps1 = add(i, vec(EPSILON, 0.0, 0.0));
	eps2 = sub(i, vec(EPSILON, 0.0, 0.0));
	n.x = (f(eps1, o)).x - (f(eps2, o)).x;
	eps1 = add(i, vec(0.0, EPSILON, 0.0));
	eps2 = sub(i, vec(0.0, EPSILON, 0.0));
	n.y = (f(eps1, o)).x - (f(eps2, o)).x;
	eps1 = add(i, vec(0.0, 0.0, EPSILON));
	eps2 = sub(i, vec(0.0, 0.0, EPSILON));
	n.z = (f(eps1, o)).x - (f(eps2, o)).x;
	return (norm(n));
}

t_vec			get_color_mb(t_obj *o)
{
	float	t;
	t_vec	tc0;

	t = powf(clamp1(o->dir.y, 0.0, 1.0), 0.55) * 4.2 + 3.0;
	tc0 = scale(vec(sin(t), sin(t + 0.5), sin(t + 1.0)), 0.5);
	tc0 = add(vec(0.5, 0.5, 0.5), tc0);
	tc0 = mult(tc0, mult(o->col, scale(vec(0.9, 0.8, 0.6), 0.2)));
	tc0 = clamp(tc0, 0.0, 1.0);
	/*tc0 = vec(powf(tc0.x, 0.45), powf(tc0.y, 0.45), powf(tc0.z, 0.45));*/
	return (tc0);
}
