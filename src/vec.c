
#include "rtv1.h"

t_vec					vec(float x, float y, float z)
{
	t_vec	v;

	v.x = x;
	v.y = y;
	v.z = z;
	v.w = 0.0;
	return (v);
}

float					length(t_vec v)
{
	return (sqrt(dot(v, v)));
}

t_vec					norm(t_vec v)
{
	return (scale(v, 1.0 / length(v)));
}
