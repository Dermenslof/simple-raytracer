
#include "rtv1.h"

float			itriangle(t_vec *i, t_vec ro, t_vec rd, t_obj *o)
{
	t_vec		e1;
	t_vec		e2;
	t_vec		p;
	t_vec		s;
	float		f;
	float		u;
	float		v;

	(void)i;
	e1 = sub(o->pos2, o->pos);
	e2 = sub(o->pos3, o->pos);
	p = cross(rd, e2);
	f = dot(e1, p);
	if (f < EPSILON)
		return (-1.0);
	f = 1.0 / f;
	s = sub(ro, o->pos);
	u = f * dot(s, p);
	if (u < EPSILON || u > 1.0)
		return (-1.0);
	p = cross(s, e1);
	v = f * dot(rd, p);
	if (v < EPSILON || u + v > 1.0)
		return (-1.0);
	o->nor = norm(cross(e1, e2));
	return (f * dot(e2, p));
}

t_vec			ntriangle(t_vec i, t_vec rd, t_obj *o)
{
	(void)i;
	(void)rd;
	return (o->nor);
}
