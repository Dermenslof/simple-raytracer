
#include "rtv1.h"

float		coherent_noise(t_vec v, int type)
{
	int		i;
	int		octave;
	float	t;
	float	sum;
	float	scl;

	octave = type == 3 ? 4 : 1;
	octave = type == 5 ? 2 : octave;
	sum = 0.0;
	scl = 1.0;
	i = -1;
	while (++i < octave)
	{
		t = noise3(v);
		sum += t / scl;
		scl *= type == 3 ? 0.3 : (type == 5 ? 0.5 : 0.4);
		v = scale(v, type == 3 ? 0.3 : (type == 5 ? 0.5 : 0.2));
	}
	return (t * (type == 5 ? 0.5 : 10.0));
}
 
t_vec		perlin_color(t_vec i, t_obj *o)
{
	float	n;
	t_vec	c;

	i = sub(i, o->pos);
	if (o->ktp == 2)
	{
		i = scale(i, 8.0);
		n = cos(i.y + coherent_noise(i, o->ktp));
	}
	else if (o->ktp == 3)
	{
		n = 20.0 * coherent_noise(i, o->ktp);
		n = fabs(n - (int)n) * M_PI;
		n = (1.0 - cos(n)) * 0.5;
	}
	else if (o->ktp == 4)
		n = coherent_noise(i, o->ktp);
	else
		n = clamp1(coherent_noise(i, o->ktp), -0.99, 0.99);
	c = add(scale(o->col, 1.0 - n), scale(o->col2, 1.0 / n));
	return (clamp(c, 0.0, 1.0));
}
