
#include "rtv1.h"

extern t_par		*g_par;

static char			*g_types[] = {
	"LIGHT",
	"SPHERE",
	"PLANE",
	"CONE",
	"CYLENDER",
	"TRIANGLE",
	"MANDELBULB",
	NULL
};

void				set_type(t_obj *o, char *s)
{
	int		i;

	i = -1;
	while (g_types[++i])
	{
		if (!ft_strcmp(g_types[i], s))
		{
			if (!ft_strcmp("LIGHT", s) && o->type)
				add_obj(&g_par->lights, pop_obj(&g_par->objects, o->id));
			else if (ft_strcmp("LIGHT", s) && !o->type)
				add_obj(&g_par->objects, pop_obj(&g_par->lights, o->id));
			o->type = i;
			return ;
		}
	}
	ft_printf("%s%s is not a valid type.\n", T_ERROR, s);
}

char				set_properties(int ac, char **av, float v, t_obj *o)
{
	if (ac == 3 && !ft_strcmp("type", av[1]))
		set_type(o, av[2]);
	else if (ac == 3 && !ft_strcmp("ka", av[1]))
		o->ka = v;
	else if (ac == 3 && !ft_strcmp("kt", av[1]))
		o->kt = v;
	else if (ac == 3 && !ft_strcmp("kd", av[1]))
		o->kd = v;
	else if (ac == 3 && !ft_strcmp("ks", av[1]))
		o->ks = v;
	else if (ac == 3 && !ft_strcmp("kl", av[1]))
		o->kl = v;
	else if (ac == 3 && !ft_strcmp("kr", av[1]))
		o->kr = v;
	else if (ac == 3 && !ft_strcmp("n", av[1]))
		o->n = v;
	else if (ac == 3 && !ft_strcmp("kch", av[1]))
		o->kch = v + 0.001;
	else if (ac == 3 && !ft_strcmp("ktp", av[1]))
		o->ktp = ft_atoi(av[2]);
	else
		return (0);
	return (1);
}

void				set_cam(int ac, char **av)
{
	t_vec		v;

	if (ac == 5)
		v = vec(ft_atof(av[2], 0), ft_atof(av[3], 0), ft_atof(av[4], 0));
	if (ac == 5 && !ft_strcmp("campos", av[1]))
		g_par->cam.pos = v;
	else if (ac == 5 && !ft_strcmp("camdir", av[1]))
	{
		g_par->cam.dir = v;
		update_cam();
	}
	else
		ft_printf("usage: %s <campos | camdir> <x> <y> <z>\n", *av);
}

void				set_obj(int ac, char **av)
{
	t_obj		*o;
	t_vec		v;

	if (g_par->id < 1)
		ft_printf("%sno object selected.\n", T_ERROR);
	else
	{
		if (ac == 5)
			v = vec(ft_atof(av[2], 0), ft_atof(av[3], 0), ft_atof(av[4], 0));
		o = get_obj(g_par->id);
		if (ac == 5 && !ft_strcmp("pos", av[1]))
			o->pos = v;
		else if (ac == 5 && !ft_strcmp("dir", av[1]))
			o->dir = v;
		else if (ac == 5 && !ft_strcmp("scale", av[1]))
			o->scale = v;
		else if (ac == 5 && !ft_strcmp("col", av[1]))
			o->col = v;
		else if (ac == 5 && !ft_strcmp("col2", av[1]))
			o->col2 = v;
		else if (ac == 3 && !ft_strcmp("radius", av[1]))
			o->r = ft_atof(av[2], 0);
		else if (ac == 3 && !set_properties(ac, av, ft_atof(av[2], 0), o))
			ft_printf("usage: %s <pos | dir | col> <x> <y> <z>\n       %s", *av\
			, " <radius | ka | kt | kd | ks | kr | kl | kch | n> <value>\n");
		set_transform(o);
		set_itransform(o);
	}
}

void				cmd_set(int ac, char **av)
{
	if (!ft_strncmp("cam", av[1], 3))
		set_cam(ac, av);
	else
		set_obj(ac, av);
	g_par->render = 1;
	g_par->status = 1;
}
