
#include "rtv1.h"

extern t_par	*g_par;

int				expose(void)
{
	IMG_TOWIN(g_par->mlx, g_par->win, g_par->img, 0, 0);
	return (0);
}

int				loop(void)
{
	if (g_par->render)
	{
		g_par->render = 0;
		render();
		render_select();
		expose();
		g_par->status = 0;
	}
	return (0);
}

int				key_pressed(int key, t_key *keys)
{
	int			i;

	if (key == K_ESC)
		rt_exit(0);
	i = -1;
	while (++i < 86)
	{
		if (keys[i].id == key && !keys[i].lock)
		{
			keys[i].state = 1;
			break ;
		}
	}
	return (0);
}

int					key_released(int key, t_key *keys)
{
	int			i;

	i = -1;
	while (++i < 86)
	{
		if (keys[i].id == key)
		{
			keys[i].state = 0;
			keys[i].lock = 0;
			break ;
		}
	}
	return (0);
}

int					mouse_released(int key, int x, int y)
{
	if (key == 1 && g_par->do_select)
		cmd_subselect(x, y);
	return (0);
}
