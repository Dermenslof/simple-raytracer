
#include "rtv1.h"

extern t_par	*g_par;

t_vec					aa(int x, int y, t_vec col)
{
	t_vec	r[2];
	t_vec	tmp;
	t_vec	ref[2];
	float	dif[2];
	int		k;

	ref[0] = get_pixel_color(x ? x - 1 : 0, y ? y : 0);
	ref[1] = get_pixel_color(x ? x : 0, y ? y - 1 : 0);
	dif[0] = fabs(col.x - ref[0].x) + fabs(col.y - ref[0].y) + fabs(col.z - ref[0].z);
	dif[1] = fabs(col.x - ref[1].x) + fabs(col.y - ref[1].y) + fabs(col.z - ref[1].z);
	k = 0;
	while (k++ < g_par->aa && (dif[0] > EPSILON || dif[1] > EPSILON))
	{
		move_cam(x, y, &r[0], &r[1]);
		tmp = trace(r[0], r[1], 0, 0);
		col = add(tmp, scale(col, (float)g_par->aa - 1.0));
		col = scale(col, 1.0 / (float)g_par->aa);
		dif[0] = fabs(col.x - ref[0].x) + fabs(col.y - ref[0].y) + fabs(col.z - ref[0].z);
		dif[1] = fabs(col.x - ref[1].x) + fabs(col.y - ref[1].y) + fabs(col.z - ref[1].z);
	}
	return (clamp(col, 0.0, 1.0));
}

void			*threading_aa(void *vit)
{
	int			x;
	int			y;
	t_vec		*it;
	t_vec		c;

	it = (t_vec *)vit;
	y = (int)it->x - 1;
	while (++y < (int)it->y)
	{
		x = -1;
		while (++x < g_par->w)
		{
			c = aa(x, y, get_pixel_color(x, y));
			plot(x, y, c);
			plot2(x, y, c);
			g_par->percent[(int)it->z]++;
		}
	}
	return (NULL);
}

void			*threading_effect(void *vit)
{
	int			x;
	int			y;
	t_vec		*it;
	t_vec		*v;

	it = (t_vec *)vit;
	y = (int)it->x - 1;
	while (++y < (int)it->y)
	{
		x = -1;
		while (++x < g_par->w)
		{
			v = g_par->buffer[y][x];
			if (v[0].x || v[0].y || v[0].z || v[1].x || v[1].y || v[1].z)
				plot2(x, y, init_trace_r(x, y, g_par->buffer[y][x]));
			g_par->percent[(int)it->z]++;
		}
	}
	return (NULL);
}

void			*threading_render(void *vit)
{
	int			x;
	int			y;
	t_vec		*it;

	it = (t_vec *)vit;
	y = (int)it->x - 1;
	while (++y < (int)it->y)
	{
		x = -1;
		while (++x < g_par->w)
		{
			plot2(x, y, init_trace(x, y));
			g_par->percent[(int)it->z]++;
		}
	}
	return (NULL);
}

void			render(void)
{
	pthread_t	th[NB_THREAD];
	t_vec		it[NB_THREAD];
	int			i;
	int			min;

	min = 0;
	i = -1;
	while (++i < NB_THREAD)
	{
		it[i].x = (float)min;
		it[i].y = (float)(min + g_par->h / NB_THREAD);
		it[i].z = (float)i;
		min += g_par->h / NB_THREAD;
		if (g_par->status == 3)
			pthread_create(&th[i], NULL, threading_effect, &it[i]);
		else if (g_par->status == 2)
			pthread_create(&th[i], NULL, threading_aa, &it[i]);
		else
			pthread_create(&th[i], NULL, threading_render, &it[i]);
	}
	i = -1;
	while (++i < NB_THREAD)
		pthread_join(th[i], NULL);
}
