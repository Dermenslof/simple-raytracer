
#include "rtv1.h"

extern t_par		*g_par;

void				init_percent(void)
{
	int		i;

	i = -1;
	while (++i < 8)
		g_par->percent[i] = 0;
}

void				draw_bar()
{
	float	percent;
	int		i[3];

	init_percent();
	i[2] = 0;
	percent = 0;
	while (percent <= 99.0)
	{
		*i = -1;
		i[1] = 0;
		while (++*i < 8)
			i[1] += g_par->percent[*i];
		percent = (i[1] * 100.0) / (float)((g_par->w * g_par->h - 1));
		if (i[2] == (int)percent)
			continue ;
		i[2] = (int)percent;
		ft_printf("\033[0G\033[0m[\033[40m\033[32m");
		*i = -1;
		while (++*i < (int)percent / 5 && percent >= 5.0)
			ft_printf("|");
		ft_printf("\033[0m");
		while ((*i)++ < 20)
			ft_printf(" ");
		ft_printf("\033[0m]");
	}
}

void				cmd_render(int ac, char **av)
{
	int		type;

	type = 1;
	if (ac >= 2)
	{
		if (!ft_strcmp("aa", av[1]) && (type = 2))
			g_par->aa = (ac > 2 && ft_atoi(av[2]) > 1) ? ft_atoi(av[2]) : 8;
		else if (!ft_strcmp("effect", av[1]) && (type = 3))
		{
			g_par->nb_pass = 1;
			if (ac > 2 && ft_atoi(av[2]) > 1)
				g_par->nb_pass = ft_atoi(av[2]);
		}
		else
		{
			ft_printf("usage: render <aa | effect> [value]\n");
			return ;
		}
	}
	g_par->render = 1;
	g_par->status = type;
	draw_bar();
}
