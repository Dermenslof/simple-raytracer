
#include "rtv1.h"

extern t_par		*g_par;

void				rt_exit(int ret)
{
	if (g_par->objects)
		free_objs(&g_par->objects);
	if (g_par->lights)
		free_objs(&g_par->lights);
	if (g_par->bgd2)
		free(g_par->bgd2);
	if (g_par->buffer)
		free(g_par->buffer);
	//buffer à free proprement
	ft_printf("see you soon !!!\n");
	exit(ret);
}

void				cmd_exit(int ac, char **av)
{
	(void)ac;
	(void)av;
	rt_exit(0);
}
