
#include "rtv1.h"

void			apply_transform(t_mat m, t_vec *ro, t_vec *rd)
{
	t_vec	rot;
	t_vec	rdt;

	matrix_apply(&rot, m, *ro);
	*ro = rot;
	if (rd)
	{
		matrix_apply(&rdt, m, *rd);
		*rd = rdt;
	}
}

void			set_transform(t_obj *o)
{
	t_mat	m;
	t_mat	tmp;
	t_mat	tmp2;
	t_mat	mt;

	matrix_id(&m);
	matrix_translate(&mt, o->pos);
	matrix_mult(&tmp, m, mt);
	matrix_rot(&mt, o->dir);
	matrix_mult(&tmp2, tmp, mt);
	matrix_scale(&mt, o->scale);
	matrix_mult(&m, tmp2, mt);
	o->trans = m;
}

void			set_itransform(t_obj *o)
{
	t_mat	m;
	t_mat	tmp;
	t_mat	tmp2;
	t_mat	mt;

	matrix_id(&m);
	matrix_itranslate(&mt, o->pos);
	matrix_mult(&tmp, m, mt);
	matrix_irot(&mt, o->dir);
	matrix_mult(&tmp2, tmp, mt);
	matrix_iscale(&mt, o->scale);
	matrix_mult(&m, tmp2, mt);
	o->itrans = m;
}
