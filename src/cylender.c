
#include "rtv1.h"

float			get_ellipse(t_vec ro, t_vec rd, t_vec *i, t_obj *o)
{
	float	t;
	float	a;

	a = -1.0;
	if (pow(ro.x, 2) + pow(ro.z, 2) <= pow(o->r, 2))
	{
		if (o->height != 0.0 && (ro.y > o->height || (ro.y >= 0.0 && ro.y < o->height)))
		{
			if (rd.y != 0.0)
			{
				t = (o->height - ro.y) / rd.y;
				*i = add(ro, scale(rd, t));
				i->y = o->height;
				if (t > EPSILON && pow(i->x, 2) + pow(i->z, 2) <= pow(o->r, 2))
					a = length(sub(ro, *i));
			}
		}
		if (o->height != 0.0 && (ro.y < 0.0 || (ro.y > 0.0 && ro.y <= o->height)))
		{
			a = -1.0;;
			if (rd.y != 0.0)
			{
				t = -ro.y / rd.y;
				*i = add(ro, scale(rd, t));
				i->y = 0.0;
				if (t > EPSILON && pow(i->x, 2) + pow(i->z, 2) <= pow(o->r, 2))
					a = length(sub(ro, *i));
			}
		}
	}
	i->w = 1.0;
	apply_transform(o->trans, i, NULL);
	return (a);
}

void			get_body(float *a, t_vec ro, t_vec rd, t_vec *i, t_obj *o)
{
	float	t;

	if (o->height != 0.0 && (i->y > o->height || (ro.y > o->height && pow(ro.x, 2) + pow(ro.z, 2) <= pow(o->r, 2))))
	{
		*a = -1.0;
		if (rd.y != 0.0)
		{
			t = (o->height - ro.y) / rd.y;
			*i = add(ro, scale(rd, t));
			i->y = o->height;
			if (t > EPSILON && pow(i->x, 2) + pow(i->z, 2) <= pow(o->r, 2))
				*a = length(sub(ro, *i));
		}
	}
	if (o->height != 0.0 && (i->y < 0.0 || (ro.y < 0.0 && pow(ro.x, 2) + pow(ro.z, 2) <= pow(o->r, 2))))
	{
		*a = -1.0;
		if (rd.y != 0.0)
		{
			t = -ro.y / rd.y;
			*i = add(ro, scale(rd, t));
			i->y = 0.0;
			if (t > EPSILON && (pow(i->x, 2) + powf(i->z, 2)) <= pow(o->r, 2))
				*a = length(sub(ro, *i));
		}
	}
}

float			icylender(t_vec *i, t_vec ro, t_vec rd, t_obj *o)
{
	float	a;
	float	b;
	float	c;
	float	d;

	apply_transform(o->itrans, &ro, &rd);
	a = pow(rd.x, 2) + pow(rd.z, 2);
	b = ro.x * rd.x + ro.z * rd.z;
	c = pow(ro.x, 2) + powf(ro.z, 2) - pow(o->r, 2);
	d = b * b - a * c;
	if (d < EPSILON)
		return (get_ellipse(ro, rd, i, o));
		/*return (-1.0);*/
	d = (-b - sqrt(d)) / a;
	c = (-b + sqrt(d)) / a;
	if (c <= EPSILON && d <= EPSILON)
		return (-1.0);
	if ((c <= d && c > EPSILON) || (d < c && d < EPSILON))
		a = c;
	else if ((d < c && d > EPSILON) || (c < d && c < EPSILON))
		a = d;
	*i = add(ro, scale(rd, a));
	a = length(sub(ro, *i));
	get_body(&a, ro, rd, i, o);
	i->w = 1.0;
	apply_transform(o->trans, i, NULL);
	return (a);
}

t_vec			ncylender(t_vec i, t_vec rd, t_obj *o)
{
	t_vec	n;

	apply_transform(o->itrans, &i, NULL);
	if (o->height == 0.0 || (o->height != 0.0 && i.y >= EPSILON && i.y <= o->height - EPSILON))
	{
		n = i;
		n.y = 0;
	}
	else if (i.y < EPSILON && i.y > -EPSILON)
		n = vec(0.0, -1.0, 0.0);
	else if (i.y < o->height + EPSILON && i.y > o->height - EPSILON)
		n = vec(0.0, 1.0, 0.0);
	n.w = 0.0;
	return (correct(i, rd, o, n));
}
