
#include "rtv1.h"

extern t_par		*g_par;

void				update_cam(void)
{
	t_mat	m;

	matrix_id(&m);
	matrix_rot(&m, g_par->cam.dir);
	g_par->cam.mrot = m;
}

void				move_cam(int x, int y, t_vec *ro, t_vec *rd)
{
	t_vec	uv;

	uv = get_uv(x, y);
	*ro = g_par->cam.pos;
	ro->w = 1.0;
	*rd = norm(vec(uv.x, uv.y, -1.0));
	matrix_apply(rd, g_par->cam.mrot, *rd);
}
