
#include "rtv1.h"

t_vec					inv(t_vec v)
{
	v.x *= -1;
	v.y *= -1;
	v.z *= -1;
	return (v);
}

t_vec					scale(t_vec v, float s)
{
	v.x *= s;
	v.y *= s;
	v.z *= s;
	return (v);
}

float					dot(t_vec v1, t_vec v2)
{
	v1 = mult(v1, v2);
	return (v1.x + v1.y + v1.z);
}

t_vec					cross(t_vec v1, t_vec v2)
{
	t_vec	r;

	r.x = v1.y * v2.z - v1.z * v2.y;
	r.y = v1.z * v2.x - v1.x * v2.z;
	r.z = v1.x * v2.y - v1.y * v2.x;
	return (r);
}
