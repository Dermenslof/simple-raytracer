
#include "rtv1.h"

static int		g_keys[87] = {
	K_ESC, K_F1, K_F2, K_F3, K_F4, K_F5, K_F6, K_F7, K_F8,
	K_F9, K_F10, K_F11, K_F12, K_SCD, K_1, K_2, K_3, K_4,
	K_5, K_6, K_7, K_8, K_9, K_0, K_CLP, K_EQUAL, K_BSPACE,
	K_TAB, K_A, K_Z, K_E, K_R, K_T, K_Y, K_U, K_I, K_O, K_P,
	K_CIRC, K_DOLLAR, K_STAR, K_Q, K_S, K_D, K_F, K_G, K_H,
	K_J, K_K, K_L, K_M, K_UCUTE, K_ENTER, K_LSHIFT, K_W, K_X,
	K_C, K_V, K_B, K_N, K_COMMA, K_SCOLON, K_COLON, K_EMARK,
	K_RSHIFT, K_LCTRL, K_ALT, K_SPACE, K_LBRCKT, K_ALTGR, K_RCTRL,
	K_LEFT, K_RIGHT, K_DOWN, K_UP, K_NUM0, K_NUM1, K_NUM2,
	K_NUM3, K_NUM4, K_NUM5, K_NUM6, K_NUM6, K_NUM7, K_NUM8,
	K_NUM9
};

t_key			*init_keys(void)
{
	t_key		*keys;
	int			i;

	if ((keys = (t_key *)malloc(sizeof(t_key) * 86)))
	{
		i = -1;
		while (++i < 87)
		{
			keys[i].id = g_keys[i];
			keys[i].state = 0;
			keys[i].lock = 0;
		}
	}
	return (keys);
}
