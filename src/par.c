
#include "rtv1.h"

extern t_par		*g_par;

void				init_param(void)
{
	g_par->do_select = 0;
	g_par->id = 0;
	g_par->render = 1;
	g_par->status = 1;
	g_par->aa = 16;
	g_par->threshold = 0.001;
	g_par->softshadow = 0.25;
	g_par->nb_pass = 5;
	g_par->cam.pos = vec(0.0, 0.0, 8.0);
	g_par->cam.dir = vec(0.0, 0.0, 0.0);
	update_cam();
	g_par->percent = malloc(sizeof(int) * NB_THREAD);
}

void				init_par(void)
{
	if (!(g_par = (t_par *)malloc(sizeof(t_par))))
		exit(1);
	g_par->w = 800;
	g_par->h = 600;
	g_par->bpp = 0;
	g_par->sl = 0;
	g_par->endian = 0;
	g_par->mlx = NULL;
	g_par->win = NULL;
	g_par->img = NULL;
	g_par->bgd = NULL;
	g_par->objects = NULL;
	g_par->lights = NULL;
	init_param();
}
