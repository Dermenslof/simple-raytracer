
#include "rtv1.h"

extern t_par			*g_par;

t_vec					trace(t_vec ro, t_vec rd, int pass, char inside)
{
	t_obj		*o;
	t_vec		i;
	t_vec		col;
	float		t;

	o = NULL;
	col = vec(0.0, 0.0, 0.0);
	if (pass < g_par->nb_pass)
	{
		o = test_intersect(&i, ro, rd, &t);
		if (o)
			col = get_pixel(i, rd, o, pass, inside);
	}
	return (col);
}

void					normal_trace(t_vec ro, t_vec rd, int pass, char inside, t_vec *col)
{
	t_obj		*o;
	t_vec		i;
	float		t;

	o = NULL;
	*col = vec(0.0, 0.0, 0.0);
	if (pass < g_par->nb_pass)
	{
		o = test_intersect(&i, ro, rd, &t);
		if (o)
			*col = get_pixel(i, rd, o, pass, inside);
	}
}

int						first_trace(t_vec ro, t_vec rd, t_vec *col, t_vec *buf)
{
	t_obj		*o;
	t_vec		i;
	float		t;

	o = NULL;
	*col = vec(0.0, 0.0, 0.0);
	buf[0] = *col;
	buf[1] = *col;
	buf[2] = *col;
	buf[3] = *col;
	o = test_intersect(&i, ro, rd, &t);
	if (o)
	{
		*col = get_first_pixel(i, rd, o, buf);
		buf[3] = *col;
		buf[4] = vec(o->kr, o->kt, 0.0);
	}
	if (o && o->id == g_par->id)
		return (o->id);
	return (-1);
}

t_vec					init_trace(int x, int y)
{
	t_vec	ro;
	t_vec	rd;
	t_vec	col;

	move_cam(x, y, &ro, &rd);
	mark_img(x, y, col, first_trace(ro, rd, &col, g_par->buffer[y][x]));
	return (col);
}

t_vec					init_trace_r(int x, int y, t_vec *buf)
{
	t_vec	colrl;
	t_vec	colrf;
	t_vec	col;

	normal_trace(buf[0], buf[1], 0, 0, &colrl);
	normal_trace(buf[0], buf[2], 0, 1, &colrf);
	colrl = scale(colrl, buf[4].x);
	colrf = scale(colrf, buf[4].y);
	col = clamp(add(add(colrl, colrf), buf[3]), 0.0, 1.0);
	mark_img(x, y, col, -2);
	return (col);
}
