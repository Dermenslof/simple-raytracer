
#include "rtv1.h"

extern t_par		*g_par;

void				init_buffer(void)
{
	int		x;
	int		y;
	t_vec	v;

	if ((g_par->buffer = malloc(sizeof(t_vec **) * g_par->h)))
	{
		y = -1;
		while (++y < g_par->h)
		{
			if ((g_par->buffer[y] = malloc(sizeof(t_vec *) * g_par->w)))
			{
				x = -1;
				while (++x < g_par->w)
				{
					if ((g_par->buffer[y][x] = malloc(sizeof(t_vec) * 5)))
					{
						v = vec(0.0, 0.0, 0.0);
						g_par->buffer[y][x][0] = v;
						g_par->buffer[y][x][1] = v;
						g_par->buffer[y][x][2] = v;
						g_par->buffer[y][x][3] = v;
						g_par->buffer[y][x][4] = v;
					}
				}
			}
		}
	}
}

void				init_mlx(void)
{
	if (!(g_par->mlx = INIT()))
		exit(2);
	if (!(g_par->win = NEW_WIN(g_par->mlx, g_par->w, g_par->h, \
					"raytracer")))
		exit(3);
	g_par->img = NEW_IMG(g_par->mlx, g_par->w, g_par->h);
	g_par->bgd = GET_DATA(g_par->img, &(g_par->bpp), &(g_par->sl), \
			&(g_par->endian));
	g_par->bgd2 = malloc(g_par->sl * g_par->h);
	init_buffer();
	g_par->keys = init_keys();
	mlx_hook(g_par->win, 2, (1L << 0), key_pressed, g_par->keys);
	mlx_hook(g_par->win, 3, (1L << 1), key_released, g_par->keys);
	mlx_hook(g_par->win, 4, (1L << 2), mouse_released, NULL);
	mlx_expose_hook(g_par->win, expose, NULL);
	mlx_loop_hook(g_par->mlx, loop, NULL);
	LOOP(g_par->mlx);
}
