
#include "rtv1.h"

t_vec		get_checker(t_vec i, t_obj *o)
{
	char	xpair;
	char	ypair;
	char	zpair;
	if (o->kch < 0.0)
		return (o->col);
	i = scale(i, 1.0 / o->kch);
	xpair = ((abs((int)i.x) % 2) == 0);
	ypair = ((abs((int)i.y) % 2) == 0);
	zpair = ((abs((int)i.z) % 2) == 0);
	if (i.x < EPSILON)
		xpair = !xpair;
	if (i.y < EPSILON)
		ypair = !ypair;
	if (i.z < EPSILON)
		zpair = !zpair;
	if (zpair)
	{
		if ((xpair && ypair) || (!xpair && !ypair))
			return (o->col);
		return (o->col2);
	}
	else if ((xpair && ypair) || (!xpair && !ypair))
		return (o->col2);
	return (o->col);
}
