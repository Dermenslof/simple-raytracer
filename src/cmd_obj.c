
#include "rtv1.h"

extern t_par		*g_par;

static char			*g_types[] = {
	"LIGHT",
	"SPHERE",
	"PLANE",
	"CONE",
	"CYLENDER",
	"TRIANGLE",
	"MANDELBULB",
	NULL
};

void				cmd_delete(int ac, char **av)
{
	(void)ac;
	(void)av;
	if (g_par->id)
	{
		if ((get_obj(g_par->id))->type)
			free(pop_obj(&g_par->objects, g_par->id));
		else
			free(pop_obj(&g_par->lights, g_par->id));
		g_par->id = 0;
		g_par->render = 1;
		g_par->status = 1;
	}
	else
		ft_printf("%sno object selected.\n", T_ERROR);
}

void				cmd_add(int ac, char **av)
{
	int		i;
	t_obj	*o;

	if (ac == 2)
	{
		i = -1;
		while (g_types[++i])
		{
			if (!ft_strcmp(g_types[i], av[1]))
			{
				o = new_obj(i);
				add_obj(i ? &g_par->objects : &g_par->lights, o);
				ft_printf("An object was added with id : %d\n", o->id);
				g_par->id = o->id;
				g_par->render = 1;
				g_par->status = 1;
				return ;
			}
		}
		ft_printf("%s%s is not a valid type.\n", T_ERROR, av[1]);
	}
	else
		ft_printf("usage: add <type>\n");
}
