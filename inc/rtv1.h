#ifndef RTV1_H
# define RTV1_H

# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <math.h>
# include <pthread.h>
# include <mlx.h>
# include <limits.h>
# include <libft.h>
# include "keys.h"

# define B				0x100
# define BM				0xff

# define N				0x100
# define NP				12   /* 2^N */
# define NM				0xfff
# define s_curve(t)		(t * t * (3. - 2. * t))

# define lerp(t, a, b)	( a + t * (b - a) )

# define setup(i,b0,b1,r0,r1)\
	t = (i == 0 ? vec.x : (i == 1 ? vec.y : vec.z)) + N;\
	b0 = ((int)t) & BM;\
	b1 = (b0+1) & BM;\
	r0 = t - (int)t;\
	r1 = r0 - 1.;

# define at2(rx,ry)		(rx * q[0] + ry * q[1])
# define at3(rx,ry,rz)	(rx * q[0] + ry * q[1] + rz * q[2])

# define INIT			mlx_init
# define NEW_WIN		mlx_new_window
# define NEW_IMG		mlx_new_image
# define GET_DATA		mlx_get_data_addr
# define IMG_TOWIN		mlx_put_image_to_window
# define HOOK			mlx_hook
# define LOOP			mlx_loop
# define XPM_TOIMG		mlx_xpm_file_to_image
# define PUT_STRING		mlx_string_put

# define NB_THREAD		(8)
# define EPSILON		(0.001)

# define RANDSHADOW(X)	((float)random() / ((float)INT_MAX / X) - X / 2.0)

# define LIGHT			(0)
# define SPHERE			(1)
# define PLANE			(2)
# define CONE			(3)
# define CYLENDER		(4)
# define TRIANGLE		(5)
# define MANDELBULB		(6)

# define T_PROMPT		"\033[1K\033[0G\033[32;4mRTV1\033[0m $> "
# define T_ERROR		"[\033[31mERROR\033[0m]: "

# define T_CLEAR		"\033[1J\033[0;0H"
# define T_RESET		"\033[0m"
# define T_FGBLACK		"\033[30m"
# define T_FGRED		"\033[31m"
# define T_FGGREEN		"\033[32m"
# define T_FGYELLOW		"\033[33m"
# define T_FGBLUE		"\033[34m"
# define T_FGPURPLE		"\033[35m"
# define T_FGCYAN		"\033[36m"
# define T_FGWHITE		"\033[37m"
# define T_FGDEFAULT	"\033[38m"
# define T_BGBLACK		"\033[40m"
# define T_BGRED		"\033[41m"
# define T_BGGREEN		"\033[42m"
# define T_BGYELLOW		"\033[43m"
# define T_BGBLUE		"\033[44m"
# define T_BGPURPLE		"\033[45m"
# define T_BGCYAN		"\033[46m"
# define T_BGWHITE		"\033[47m"
# define T_BGDEFAULT	"\033[48m"

# define T_ITALIC		"\033[3m"
# define T_UNDERLINE	"\033[4m"
# define T_NITALIC		"\033[23m"
# define T_NUNDERLINE	"\033[24m"

typedef struct s_par	t_par;
typedef struct s_vec	t_vec;
typedef struct s_obj	t_obj;
typedef struct s_cmd	t_cmd;
typedef struct s_set	t_set;
typedef struct s_mat	t_mat;
typedef struct s_cam	t_cam;

struct					s_mat
{
	float	v[16];
};

struct					s_set
{
	char	*name;
	void	(*func)(int);
};

struct					s_cmd
{
	char	*name;
	void	(*func)(int, char **);
};

struct					s_vec
{
	float	x;
	float	y;
	float	z;
	float	w;
};

struct					s_cam
{
	t_vec	pos;
	t_vec	dir;
	t_mat	mrot;
};

struct					s_obj
{
	int			id;
	int			type;
	t_vec		pos;
	t_vec		pos2;
	t_vec		pos3;
	t_vec		dir;
	t_vec		scale;
	t_vec		col;
	t_vec		col2;
	t_vec		nor;
	float		height;
	float		r;
	float		ka; // coef lumiere ambiante
	float		kd; // coef lumiere diffuse
	float		ks; // coef lumiere spectrale
	float		kl; // coef de brillance
	float		kr; // coef de lumiere reflechie
	float		kt; // coef de lumiere refracte
	float		n;  // indice de refraction
	float		kch;// taille du checker
	int			ktp;// type de couleur
	t_mat		trans;
	t_mat		itrans;
	t_obj		*next;
};

struct					s_par
{
	t_key		*keys;
	int			w;
	int			h;
	int			bpp;
	int			sl;
	int			endian;
	void		*mlx;
	void		*win;
	void		*img;
	char		*bgd;
	char		*bgd2;
	t_cam		cam;
	float		softshadow;
	int			aa;
	char		render_aa;
	float		threshold;
	t_obj		*objects;
	t_obj		*lights;
	char		do_select;
	int			id;
	char		render;
	int			nb_pass;
	int			status;
	int			*percent;
	t_vec		***buffer;
};

/*
** par.c
*/
void					init_par(void);

/*
** mlx.c
*/
void					init_mlx(void);

/*
** kook.c
*/
int						expose(void);
int						loop(void);
int						key_pressed(int key, t_key *keys);
int						key_released(int key, t_key *keys);
int						mouse_released(int key, int x, int y);

/*
** vec.c
*/
t_vec					vec(float x, float y, float z);
float					length(t_vec v);
t_vec					norm(t_vec v);

/*
** vec_math.c
*/
t_vec					add(t_vec v1, t_vec v3);
t_vec					sub(t_vec v1, t_vec v3);
t_vec					mult(t_vec v1, t_vec v3);
t_vec					divide(t_vec v1, t_vec v3);

/*
** util_vec.c
*/
t_vec					inv(t_vec v);
t_vec					scale(t_vec v, float s);
float					dot(t_vec v1, t_vec v3);
t_vec					cross(t_vec v1, t_vec v2);

/*
** util_ray.c
*/
t_vec					get_uv(int x, int y);
t_obj					*test_intersect(t_vec *i, t_vec ro, t_vec rd, float *t);
t_vec					refract(t_vec rd, t_vec nor, float n, char *inside);
t_vec					reflect(t_vec rd, t_vec n);

/*
** obj.c
*/
t_obj					*new_obj(int type);
void					add_obj(t_obj **base, t_obj *o);
void					free_objs(t_obj **o);
t_obj					*get_obj(int id);
t_obj					*pop_obj(t_obj **base, int id);

/*
** cmd_info.c
*/
void					print_object(t_obj *o, char prompt);

/*
** render.c
*/
void					render(void);
void					*threading(void *vit);

/*
** util_pixel.c
*/
void					plot(int x, int y, t_vec c);
void					plot2(int x, int y, t_vec c);
t_vec					get_pixel_color(int x, int y);
void					mark_img(int x, int y, t_vec c, int id);

/*
** ray.c
*/
t_vec					init_trace(int x, int y);
t_vec					init_trace_r(int x, int y, t_vec *buf);
t_vec					trace(t_vec ro, t_vec rd, int pass, char inside);
void					normal_trace(t_vec ro, t_vec rd, int pass, char inside, t_vec *col);
int						first_trace(t_vec ro, t_vec rd, t_vec *col, t_vec *buf);

/*
** obj_func.c
*/
void					apply_transform(t_mat m, t_vec *ro, t_vec *rd);
void					set_transform(t_obj *o);
void					set_itransform(t_obj *o);
t_vec					get_normal(t_vec i, t_vec rd, t_obj *o);
float					get_intersect(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					correct(t_vec i, t_vec rd, t_obj *o, t_vec n);

/*
** sphere.c
*/
float					isphere(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					nsphere(t_vec i, t_vec rd, t_obj *o);

/*
** plane.c
*/
float					iplane(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					nplane(t_vec i, t_vec rd, t_obj *o);

/*
** cylender.c
*/
float					icylender(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					ncylender(t_vec i, t_vec rd, t_obj *o);

/*
** cone.c
*/
float					icone(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					ncone(t_vec i, t_vec rd, t_obj *o);

/*
** triangle.c
*/
float					itriangle(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					ntriangle(t_vec i, t_vec rd, t_obj *o);

/*
** mandelbulb.c
*/
float					imandelbulb(t_vec *i, t_vec ro, t_vec rd, t_obj *o);
t_vec					nmandelbulb(t_vec i, t_vec rd, t_obj *o);
t_vec					get_color_mb(t_obj *o);

/*
** util_color.c
*/
t_vec					clamp(t_vec c, float min, float max);
float					clamp1(float f, float min, float max);

/*
** color.c
*/
t_vec					get_pixel(t_vec i, t_vec rd, t_obj *o, int pass, char inside);
t_vec					get_first_pixel(t_vec i, t_vec rd, t_obj *o, t_vec *buf);

/*
** cmd_parser.c
*/
void					parse_cmd(int ac, char **av);

/*
** cmd_obj.c
*/
void					cmd_add(int ac, char **av);
void					cmd_delete(int ac, char **av);

/*
** cmd_render.c
*/
void					cmd_render(int ac, char **av);

/*
** cmd_select.c
*/
void					render_select(void);
void					cmd_select(int ac, char **av);
void					cmd_subselect(int x, int y);
void					cmd_unselect(int ac, char **av);

/*
** cmd_info.c
*/
void					cmd_info(int ac, char **av);

/*
** cmd_set.c
*/
void					cmd_set(int ac, char **av);

/*
** cmd_exit.c
*/
void					cmd_exit(int ac, char **av);
void					rt_exit(int ret);

/*
** util_cmd.c
*/
int						select_obj(int x, int y);
float					ft_atof(char *str, int i);
int						count_split(char **split);
void					free_split(char **split);

t_vec					get_checker(t_vec i, t_obj *o);

void					render_select(void);
void					render_effect(int type);
void					mark_select(int x, int y, t_vec c);

/*
** perlin.c
*/
float					noise3(t_vec vec);

/*
** util_perlin.c
*/
t_vec					perlin_color(t_vec i, t_obj *o);

/*
** matrix.c
*/
void					print_matrix(t_mat *m);
void					matrix_id(t_mat *m);
void					matrix_apply(t_vec *v, t_mat a, t_vec b);
void					matrix_translate(t_mat *m, t_vec v);
void					matrix_scale(t_mat *m, t_vec v);
void					matrix_itranslate(t_mat *m, t_vec v);
void					matrix_iscale(t_mat *m, t_vec v);
void					matrix_rotx(t_mat *m, float a);
void					matrix_roty(t_mat *m, float a);
void					matrix_rotz(t_mat *m, float a);
void					matrix_mult(t_mat *m, t_mat m1, t_mat m2);
void					matrix_rot(t_mat *m, t_vec v);
void					matrix_irot(t_mat *m, t_vec v);

/*
** camera.c
**/
void					update_cam(void);
void					move_cam(int x, int y, t_vec *ro, t_vec *rd);

#endif
